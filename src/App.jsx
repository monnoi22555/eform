import Register from './pages/Register'
import Login from './pages/Login'
import Home from './pages/Home'
import Form from './pages/Form'
import Form2 from './pages/Form2'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/form">
          <Form />
        </Route>
        <Route path="/form2">
          <Form2 />
        </Route>
        <Route path="/">
          <Home/>
        </Route>
      </Switch>
    </Router>
  )
}

export default App
