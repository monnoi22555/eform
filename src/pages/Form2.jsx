import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import moment from 'moment';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';




function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
     
    </Typography>
  );
}


const theme = createTheme();



export default function Login() {
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    // eslint-disable-next-line no-console
    console.log({
      email: data.get('email'),
      password: data.get('password'),
    });
    window.location = '/Form2';
  };

  
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexWrap: 'wrap',
              '& > :not(style)': {
                m: 1,
                width: 600,
                height: 130,
                alignItems: 'center',
              },
            }}
          >
            <Paper elevation={3}> <Typography  component="h1" variant="h5">
             รายงานผลการตรวจโรงงาน
            </Typography></Paper>
          </Box>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <div component="h1" variant="h4">{moment().format('L')}</div>
          <p>
          </p>
          <div component="h1" variant="h3"> ข้อมูลการผลิต </div>
          <div component="h5" variant="h5"> คนงานชาย <TextField
              margin="normal"
              required
              fullWidth
              id="Male worker"
              label="คนงานชาย"
              name="Male worker"
              autoComplete="current-Male worker"
              autoFocus/></div>
              <div component="h5" variant="h5"> คนงานหญิง </div>
            <TextField
              margin="normal"
              required
              fullWidth
              name="Female worker"
              label="ระบุข้อมูล"
              type="Female worker"
              id="Female worker"
              autoComplete="current-Female worker"
            />
            
            <div component="h5" variant="h5">ชำระค่าธรรมเนียมรายปี</div>
            <FormControlLabel control={<Checkbox value="ครบถ้วน" color="primary" />}label="ครบถ้วน"/> <FormControlLabel control={<Checkbox value="ไม่ครบถ้วน" color="primary" />}label="ไม่ครบถ้วน"/>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              ต่อไป
            </Button>
          </Box>
        </Box>
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </ThemeProvider>
  );
}